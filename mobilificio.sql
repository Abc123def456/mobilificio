DROP DATABASE IF EXISTS mobilificio;
CREATE DATABASE mobilificio;
USE mobilificio;

create table Mobile(
mobileID INTEGER NOT NULL AUTO_INCREMENT PRIMARY KEY,
	nome VARCHAR(200) NOT NULL, -- INTEGER NOT NULL AUTO_INCREMENT PRIMARY KEY,
    descrizione VARCHAR(250) NOT NULL,
	codice VARCHAR(250) NOT NULL UNIQUE,
    prezzo FLOAT 
);

create table Categoria(
categoriaID INTEGER NOT NULL AUTO_INCREMENT PRIMARY KEY,
nome VARCHAR(200) NOT NULL,
descrizione VARCHAR(250),
codice VARCHAR(50) NOT NULL UNIQUE
);

INSERT INTO Mobile(nome,descrizione,codice,prezzo)VALUES
("Sedia mor","Sedia da ufficio con schienale","sed23",67.00),
("Tavolo ping pong","tavolo accessoriato","tavpp",89.00),
("Armadio fri","Armadio 6 ante","arn45",178.59),
("Letto bui","Latto una piazza e mezzo","le15",49.00);

INSERT INTO Categoria (nome,descrizione,codice) VALUES
("Camera","zona notte","cn2"),
("Studio","zona giornoe","en2"),
("Salotto","zona giorno","562"),
("Cucina","zona gieono","453"),
("Svago","zona noefe","c34f");

select * from categoria;

Create table mobilicategorie(
mobileid integer not null,
categoriaid integer not null,
foreign key(mobileid) references Mobile(mobileID) on delete cascade,
foreign key(categoriaid) references Categoria(categoriaID) on delete cascade,
primary key (mobileid,categoriaid)
);

insert into mobilicategorie(mobileid,categoriaid) values 
(1,2),
(1,4),
(3,1),
(4,1),
(4,3),
(2,5);

SELECT Categoria.categoriaID, Categoria.nome, Categoria.descrizione, Categoria.codice,
       			 Mobile.mobileID, Mobile.nome, Mobile.codice, Mobile.prezzo FROM Categoria
       				JOIN mobilicategorie ON Categoria.categoriaID=mobilicategorie.categoriaid
       			 JOIN Mobile ON mobilicategorie.mobileid =Mobile.mobileID;