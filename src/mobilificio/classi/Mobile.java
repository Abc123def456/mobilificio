package mobilificio.classi;

import java.util.ArrayList;

public class Mobile {

	private Integer id;
	private String nome;
	private String descrizione;
	private String codice;
	private Float prezzo;
	private ArrayList<Categoria> cat = new ArrayList<Categoria>();

	public Mobile() {

	}

	public Mobile(String nome, String descrizione, String codice, Float prezzo, ArrayList<Categoria> cat) {
		super();
		this.nome = nome;
		this.descrizione = descrizione;
		this.codice = codice;
		this.prezzo = prezzo;
		this.cat = cat;
	}

	
	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public String getDescrizione() {
		return descrizione;
	}

	public void setDescrizione(String descrizione) {
		this.descrizione = descrizione;
	}

	public String getCodice() {
		return codice;
	}

	public void setCodice(String codice) {
		this.codice = codice;
	}

	public Float getPrezzo() {
		return prezzo;
	}

	public void setPrezzo(Float prezzo) {
		this.prezzo = prezzo;
	}

	public ArrayList<Categoria> getCat() {
		return cat;
	}

	public void setCat(Categoria cat) {
		this.cat.add(cat);
	}

	@Override
	public String toString() {
		String cate="";
		for (int i=0; i<cat.size();i++) {
			cate+=(cat.get(i)).getNome()+(cat.get(i)).getDescrizione()+(cat.get(i)).getCodice();
		}
		return cate;
	}



}
