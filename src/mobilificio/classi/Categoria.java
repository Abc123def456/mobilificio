package mobilificio.classi;

public class Categoria {

	private Integer id;
	private String nome;
	private String descrizione;
	private String codice;

	public Categoria() {

	}

	public Categoria(String nome, String descrizione, String codice) {
		super();
		this.nome = nome;
		this.descrizione = descrizione;
		this.codice = codice;
	}

	
	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public String getDescrizione() {
		return descrizione;
	}

	public void setDescrizione(String descrizione) {
		this.descrizione = descrizione;
	}

	public String getCodice() {
		return codice;
	}

	public void setCodice(String codice) {
		this.codice = codice;
	}

	@Override
	public String toString() {
		return "Categoria [nome=" + nome + ", descrizione=" + descrizione + ", codice=" + codice + "]";
	}

	
}
