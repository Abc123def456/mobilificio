package mobilificio.servlets;

import java.io.IOException;
import java.io.PrintWriter;
import java.sql.SQLException;
import java.util.ArrayList;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.google.gson.Gson;

import mobilificio.classi.Categoria;
import mobilificio.dao.CategoriaDAO;

@WebServlet("/InserisciCategoria")
public class InserisciCategoria extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		CategoriaDAO catdao = new CategoriaDAO();
		
		try {
			ArrayList<Categoria> elenco = catdao.getAll();

			Gson jsonizzatore = new Gson();
			String risultatoJson = jsonizzatore.toJson(elenco);

			
			PrintWriter out = response.getWriter();
			out.print(risultatoJson);
			
		} catch (SQLException e) {

			System.out.println(e.getMessage());
			
		}
		
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
	}

}
