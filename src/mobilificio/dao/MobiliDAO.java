package mobilificio.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

import mobilificio.classi.Categoria;
import mobilificio.classi.Mobile;
import mobilificio.connessione.ConnettoreDB;

public class MobiliDAO {

	public ArrayList<Mobile> getAll() throws SQLException {

		ArrayList<Mobile> elencomob = new ArrayList<Mobile>();
	       
   		Connection conn = ConnettoreDB.getIstanza().getConnessione();
           
       	String query = "SELECT nome, descrizione, codice, prezzo FROM Mobile";
       	PreparedStatement ps = (PreparedStatement) conn.prepareStatement(query);
       	
       	ResultSet risultato = ps.executeQuery();
       	while(risultato.next()){
       		Mobile temp = new Mobile();
       		temp.setNome(risultato.getString(1));
       		temp.setDescrizione(risultato.getString(2));
       		temp.setCodice(risultato.getString(3));
       		temp.setPrezzo(risultato.getFloat(4));
       		elencomob.add(temp);
       	}
       	
       	return elencomob;
	}
	
	public ArrayList<Mobile> AssegnaCategoria() throws SQLException {

   		ArrayList<Mobile> elencomob = new ArrayList<>();
		Connection conn = ConnettoreDB.getIstanza().getConnessione();
        
       	String query = "SELECT Categoria.categoriaID, Categoria.nome, Categoria.descrizione, Categoria.codice,\r\n"
       			+ "       			 Mobile.mobileID, Mobile.nome, Mobile.descrizione,Mobile.codice, Mobile.prezzo FROM Categoria\r\n"
       			+ "       				JOIN mobilicategorie ON Categoria.categoriaID=mobilicategorie.categoriaid\r\n"
       			+ "       			 JOIN Mobile ON mobilicategorie.mobileid =Mobile.mobileID";
       	PreparedStatement ps = (PreparedStatement) conn.prepareStatement(query);
       	
       	ResultSet risultato = ps.executeQuery();
       	
       	while(risultato.next()){
       		Categoria ca = new Categoria();
       		ca.setId(risultato.getInt(1));
       		ca.setNome(risultato.getString(2));
       		ca.setDescrizione(risultato.getString(3));
       		ca.setCodice(risultato.getString(4));

       		Mobile temp = new Mobile();
       		temp.setId(risultato.getInt(5));
       		temp.setNome(risultato.getString(6));
       		temp.setDescrizione(risultato.getString(7));
       		temp.setCodice(risultato.getString(8));
       		temp.setPrezzo(risultato.getFloat(9));
   			
       		if(elencomob.size()>0) {
       			for(int i=0;i<elencomob.size();i++) {
       				if(temp.getId()==elencomob.get(i).getId()) {
       					elencomob.get(i).setCat(ca);
       				}else {
       					temp.setCat(ca);
       					elencomob.add(temp);
       				}
       			}
       		}else {
       			temp.setCat(ca);
       			elencomob.add(temp);
       		} 
       	}
       	
       	return elencomob;
	}
	
	
}
