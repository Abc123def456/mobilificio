package mobilificio.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

import mobilificio.classi.Categoria;
import mobilificio.connessione.ConnettoreDB;

public class CategoriaDAO {

	
	public Categoria getByCodice(String codice) throws SQLException {
		// TODO Auto-generated method stub
		return null;
	}

	
	public ArrayList<Categoria> getAll() throws SQLException {

		ArrayList<Categoria> elencocat = new ArrayList<Categoria>();
	       
   		Connection conn = ConnettoreDB.getIstanza().getConnessione();
           
       	String query = "SELECT nome, descrizione, codice FROM Categoria";
       	PreparedStatement ps = (PreparedStatement) conn.prepareStatement(query);
       	
       	ResultSet risultato = ps.executeQuery();
       	while(risultato.next()){
       		Categoria temp = new Categoria();
       		temp.setNome(risultato.getString(1));
       		temp.setDescrizione(risultato.getString(2));
       		temp.setCodice(risultato.getString(3));
       		elencocat.add(temp);
       	}
       	
       	return elencocat;
	}


	public void insert(Categoria t) throws SQLException {
		Connection conn = ConnettoreDB.getIstanza().getConnessione();
        
       	String query = "INSERT INTO Categoria (nome,descrizione,prezzo) VALUES (?,?,?)";
       	PreparedStatement ps = (PreparedStatement) conn.prepareStatement(query);
       	ps.setString(1, t.getNome());
		ps.setString(3, t.getCodice());
		ps.setString(2, t.getDescrizione());
		
		ps.executeUpdate();
		ResultSet risultato = ps.getGeneratedKeys();
		risultato.next();

	}


	public boolean delete(Categoria t) throws SQLException {
		// TODO Auto-generated method stub
		return false;
	}

	
	public boolean update(Categoria t) throws SQLException {
		// TODO Auto-generated method stub
		return false;
	}
}
