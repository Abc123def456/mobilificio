
function stampaCategoria(arr_cat){
	let contenuto = "";
	
	for(let i=0; i<arr_cat.length; i++){
		contenuto += Risultato(arr_cat[i]);
	}
	
	$("#contenuto-richiesta").html(contenuto);
}

function Risultato(obj_cat){
	
	let risultato = '<tr>';
	risultato += '    <td>' + obj_cat.nome + '</td>';
	risultato += '    <td>' + obj_cat.descrizione+ '</td>';
	risultato += '    <td>' + obj_cat.codice+ '</td>';
	risultato += '</tr>';
	
	return risultato;
	
}

function aggiornaTabella(){
	$.ajax(
			{
				url: "http://localhost:8080/Mobilificio/InserisciCategoria",
				method: "POST",
				success: function(ris_success){
					
					let risJson = JSON.parse(ris_success);	
					stampaCategoria(risJson);
				},
				error: function(ris_error){
					console.log("ERRORE");
					console.log(ris_error);
				}
			}
	);
}

$(document).ready(
		function(){

			aggiornaTabella();	
			}
);