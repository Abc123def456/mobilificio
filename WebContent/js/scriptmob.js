
function stampaMobili(arr_mob){
	let contenuto = "";
	
	for(let i=0; i<arr_mob.length; i++){
		contenuto += Risultato(arr_mob[i]);
	}
	
	$("#contenuto-mobili").html(contenuto);
}

function Risultato(obj_mob){
	
	let risultato = '<tr data-identificatore="' + obj_mob.codice + '">';
	risultato += '    <td>' + obj_mob.nome + '</td>';
	risultato += '    <td>' + obj_mob.descrizione+ '</td>';
	risultato += '    <td>' + obj_mob.codice+ '</td>';
	risultato += '    <td>' + obj_mob.prezzo+ '</td>';
	risultato += '    <td>' + obj_mob.toString()+ '</td>';
	//risultato += '    <td><button type="button" class="btn btn-block" onclick="dettaglio(this)>Dettaglio</button>"</td>';
	risultato += '</tr>';
	
	return risultato;
	
}

function aggiornaMobili(){
	$.ajax(
			{
				url: "http://localhost:8080/Mobilificio/MobileCategoria",
				method: "POST",
				success: function(ris_success){
					
					let risJson = JSON.parse(ris_success);	
					stampaMobili(risJson);
				},
				error: function(ris_error){
					console.log("ERRORE");
					console.log(ris_error);
				}
			}
	);
}



$(document).ready(
		function(){

			aggiornaMobili();	
			}
);